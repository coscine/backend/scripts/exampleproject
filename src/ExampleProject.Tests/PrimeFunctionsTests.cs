﻿using NUnit.Framework;

namespace Coscine.ExampleProject.Tests
{
    [TestFixture]
    public class PrimeFunctionsTests
    {
        private readonly PrimeFunctions _primeFunctions;

        public PrimeFunctionsTests()
        {
            _primeFunctions = new PrimeFunctions();
        }

        [Test]
        public void TestMethod1()
        {
            Assert.IsTrue(_primeFunctions.IsPrime(2));
        }

        [Test]
        public void TestMethod2()
        {
            Assert.IsTrue(_primeFunctions.IsPrime(13));
        }

        [Test]
        public void TestMethod3()
        {
            Assert.IsFalse(_primeFunctions.IsPrime(6));
        }
    }
}
