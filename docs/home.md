## C# Example

This example includes:

* Automatic building using cake
* Automatic testing with NUnit
* Automatic linting with Resharper
* Automatic documentation publishing using Gitlab CI / CD and a self written script which puts the docs in the docs folder to the wiki
* Automatic releases using semantic-release ([ESLint Code Convention](ESLintConvention)), cake and Gitlab CI / CD

## Building

Build this project by running either the build.ps1 or the build<span></span>.sh script.
The project will be build and tested.

### Links 

*  [Commit convention](ESLintConvention)
*  [Everything possible with markup](testdoc)
*  [Adding NUnit tests](nunit)