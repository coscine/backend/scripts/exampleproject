## C# Example

This example includes:

* Automatic building using cake
* Automatic testing with NUnit
* Automatic linting with Resharper
* Automatic documentation publishing using Gitlab CI / CD and a self written script which puts the docs in the docs folder to the wiki
* Automatic releases using semantic-release ([ESLint Code Convention](docs/ESLintConvention.md)), cake and Gitlab CI / CD

## Building

Build this project by running either the build.ps1 or the build<span></span>.sh script.
The project will be build and tested.
Default will build and test with Release.

### Links 

*  [Commit convention](docs/ESLintConvention.md)
*  [Everything possible with markup](docs/testdoc.md)
*  [Adding NUnit tests](docs/nunit.md)